function psychometric

%  Auditory headfixed go/no-go protocol for 2 or more sounds predicting reward (water) or
%  punihsment (air-puff)

% Written by B�lint Kir�ly 23/04/2018
% Hungarian Academy of Sciences, Institute of Experimental Medicine
% Lend�let Laboratory of Systems Neuroscience (Bal�zs Hangya)

global BpodSystem

%% Define parameters
S = BpodSystem.ProtocolSettings; % Load settings chosen in launch manager into current workspace as a struct called S
S = struct;
if isempty(fieldnames(S)) % If settings file was an empty struct, populate struct with default settings
    %GUI parameters
    S.GUI.Phase=5; %2/3/4/5 (phase #1 is free water)
    S.GUI.FreeWater_sum = 0; %10/5/2/0
    S.GUI.FreeWater=0;
    S.GUI.Drinked_ul=0;
    S.GUI.HitNum=0;
    S.HitNum_req=50; %50 required number of actual hits for moving forward to phase 3
    S.GUI.HitRatio1=0;
    S.GUI.HitRatio2=0;
    S.GUI.HitRatio3=0;
    S.GUI.HitRatio_norm1=0;
    S.GUI.HitRatio_norm2=0;
    S.GUI.HitRatio_norm3=0;
    S.HitRatio_req=70; %    70%
    %Task parameters
    S.SinWavekHz=[4,10,8]; % tones in kHz. #1 is used for phase 2-3 %7.5
    S.RandomSounds=[5,6,7,8,9];
    S.GUI.thrdSound=S.SinWavekHz(end);
    S.TrialType=[1,2,2]; % Trial types coded by the tones in S.SinWavekHz (1=reward,2=punish)
    S.TrialProb=[0.5,0.5]; % probability of the tones in the order in S.SinWavekHz
    if S.TrialType==[1,2,2]
       S.TrialProb = [0.5,0.25,0.25]
    elseif S.TrialType==[1,2,1]
       S.TrialProb=[0.25,0.5,0.25]
    end
    S.TrialTypenum=length(S.SinWavekHz);   
    S.MaxTrials=1000;
    S.MaxTrials=10+S.MaxTrials; % virtulaly raise the number of Trials for the trick used for plotting 
    % time intervals
    S.NoLick = 1.5; % s
    S.ITI = 1; % s
    S.ITI_max=4;
    S.Delay=30; % s
    S.RewardDelay=0;
    S.SoundDuration = 0.6; %s
    S.SinWavedB = 60; % Cue tone dB SPL
    % port settings   
    S.RewardValveCode = 1;   % port #1 controls water valve
    S.PunishValveCode = 4;   % port #3 controls air valve
    S.RewardAmount = 3; % ul
    S.PunishValveTime = 0.2; % s
    S.RewardValveTime =  GetValveTimes(S.RewardAmount, S.RewardValveCode);
end
BpodParameterGUI('init', S); % Initialize parameter GUI plugin
rng('shuffle')   % reset pseudorandom seed
currentTrial=1;
%% sound initialization
TeensySoundServer ( 'init' , 'COM7' );
FilePath = fullfile(BpodSystem.BpodPath,'Protocols','TeensyCalibration','TeensyCalData.mat');
load(FilePath);  %load calibration data
for i=1:S.TrialTypenum
  soundinitializer (S.SinWavedB,S.SinWavekHz(i),TeensyCalData,S.SoundDuration); %initializing sound for every trialtypes
end

%% Define trials
    for i=1:S.MaxTrials
        p=rand(1);
        prob=0;
        j=1;
        if S.GUI.Phase==2 | S.GUI.Phase==3
           TrialTypes(i)=1;                     %generating random TrialTypes with the probabilities given in S.TrialProb
        else
        while prob<p
        prob=prob+S.TrialProb(j);
        TrialTypes(i)=j;
        j=j+1;
        end
    end
end
        

%% Initialize plots
BpodSystem.ProtocolFigures.OutcomePlotFig = figure('Position', [200 200 1000 200],'name','Outcome plot','numbertitle','off', 'MenuBar', 'none', 'Resize', 'off');
BpodSystem.GUIHandles.OutcomePlot = axes('Position', [.075 .3 .89 .6]);
TrialTypeOutcomePlot(BpodSystem.GUIHandles.OutcomePlot,'init',TrialTypes);
BpodSystem.Data.TrialTypes = [];
BpodSystem.Data.TrialCode = [];
BpodSystem.Data.thrdSound = [];
    %% free water
for currentTrial = 1:S.GUI.FreeWater_sum
    
    if S.TrialType(end)==1 % Type1 -- Reward
            StateChangeArgument0 = 'Reward';
            Freq0 = S.SinWavekHz(end);
    else S.TrialType(end)==2 % Type2 -- Punishment
            StateChangeArgument0 = 'Reward';
            Freq0 = S.SinWavekHz(1);
    end

    sma = NewStateMatrix();   
    sma = AddState(sma, 'Name', 'start', ...
        'Timer',1,...
        'StateChangeConditions', {'Tup', 'StartStimulus'},...
        'OutputActions', {'PWM1', 255});   % 0.3 + exponential foreperiod < 4s
    sma = AddState(sma, 'Name', 'StartStimulus', ... 
        'Timer', S.SoundDuration,...
        'StateChangeConditions', {'Tup',StateChangeArgument0},...
        'OutputActions', {'Serial1Code', Freq0,});   % play tone
    sma = AddState(sma,'Name', 'Reward', ...
        'Timer',S.RewardValveTime,...
        'StateChangeConditions', {'Tup', 'PostUS'},...
        'OutputActions', {'ValveState', S.RewardValveCode});   % deliver water
    sma = AddState(sma, 'Name', 'Punish', ...
        'Timer',S.PunishValveTime, ...
        'StateChangeConditions', {'Tup', 'PostUS'}, ...
        'OutputActions', {'ValveState', S.PunishValveCode}); %deiliver air puff
    sma = AddState(sma,'Name','PostUS',...
        'Timer',1,...
        'StateChangeConditions',{'Port1In','ResetDrinkingTimer','Tup','exit'},...
        'OutputActions',{});   % drinking
    sma = AddState(sma,'Name','ResetDrinkingTimer',...
        'Timer',0,...
        'StateChangeConditions',{'Tup','PostUS'},...
        'OutputActions',{});   % keep the animal in PostUS until licking stops for 1 s
    SendStateMatrix(sma);    
    RawEvents = RunStateMatrix;
    GUIHandler('FreeWater',currentTrial)
    if currentTrial == S.GUI.FreeWater_sum
        continue
    end
    HandlePauseCondition; % Checks to see if the protocol is paused. If so, waits until user resumes.
    if BpodSystem.BeingUsed == 0
        return
    end
end
    
    %% phase 2,3,4
    c=0;
    d=0;
    e=0;
    r=0;
    hitratio1=0;
    hitratio2=0;
    hitratio3=0;
    temp1=0;
    temp2=0;
    temp3=0;
    currentTrial=1;
    S.GUI.HitNum=0; %in phase 2: number of actual hits
    BpodSystem.Data.Outcomes=3;
    while(currentTrial<S.MaxTrials-10) %plotting dies if we reach the last trials, so we finish earlier
        
        if S.GUI.HitNum==S.HitNum_req   %if the required number of actual hits are made, move on to phase 3
            S.GUI.Phase=3;
        end
        
        if S.GUI.Phase>2            % move on to phase 3
            S.Delay=0;
            S.RewardDelay=gaussdelay(0.2,0.03,0.1,0.3);        % random delay of the reward and the punishment (normal distribution between 0.1s and 0.3s)
        end
        
         if S.GUI.Phase==5
             if S.GUI.HitRatio3>=0.7 && S.GUI.HitRatio_norm3>=33 && S.GUI.HitRatio2>=0.7 && S.GUI.HitRatio1>=0.7
                r=r+1;
                oldS=S.SinWavekHz(end);
                randsound=oldS;
                while randsound>=oldS-0.5 && randsound<=oldS+0.5
                randsound=S.RandomSounds(randi(length(S.RandomSounds)));
                end
                soundinitializer (S.SinWavedB,randsound,TeensyCalData,S.SoundDuration);
                S.SinWavekHz=[4,10,randsound]; % tones in kHz. #1 is used for phase 2-3 %7.5
                GUIHandler('thrdSound',randsound)
                if rand(1)<0.5
                S.TrialProb=[0.25,0.5,0.25];
                S.TrialType=[1,2,1];
                else
                S.TrialProb=[0.5,0.25,0.25];
                S.TrialType=[1,2,2];
                end
                c=0;
                d=0;
                e=0;
                hitratio1=0;
                hitratio2=0;
                hitratio3=0;
                temp1=0;
                temp2=0;
                temp3=0;
                    for i=currentTrial:S.MaxTrials
                        p=rand(1);
                        prob=0;
                        j=1;
                        while prob<p
                        prob=prob+S.TrialProb(j);
                        TrialTypes(i)=j;
                        j=j+1;
                        end
                end

                 
             end
         end
                 
             
             
             
        
    j=1;
    while j<=S.TrialTypenum %loads the parameters 
    if TrialTypes(currentTrial)==j 
        Freq = S.SinWavekHz(j);
        if S.TrialType(j)==1 % Type1 -- Reward
            q=1;
            StateChangeArgument1 = 'Reward';  
            StateChangeArgument2 = 'PostUS';
        else S.TrialType(j)==2 % Type2 -- Punishment
            q=2;
            StateChangeArgument1 = 'Punish';  
            StateChangeArgument2 = 'PostUS';
        end
    end
    j=j+1;
    end
    
    S.ITI=expdelay(S.ITI_max); % random (0.3+exponential distribution,)

            GUIHandler('Phase', S.GUI.Phase)
            
            
            sma = NewStateMatrix();
            sma = AddState(sma,'Name', 'NoLick', ...
                'Timer', S.NoLick,...
                'StateChangeConditions', {'Tup', 'ITI','Port1In','RestartNoLick'},...
                'OutputActions', {'PWM1', 255}); % Light On
            sma = AddState(sma,'Name', 'RestartNoLick', ...
                'Timer', 0,...
                'StateChangeConditions', {'Tup', 'NoLick',},...
                'OutputActions', {'PWM1', 0}); % Light On; keep the animal in no lick state until 1.5s pause in licking
            sma = AddState(sma, 'Name', 'ITI', ...
                'Timer',S.ITI,...
                'StateChangeConditions', {'Tup', 'StartStimulus', 'Port1In','RestartNoLick'},...
                'OutputActions', {});  % 0.3 + exponential foreperiod < 4s
            sma = AddState(sma, 'Name', 'StartStimulus', ... 
                'Timer', S.SoundDuration,...
                'StateChangeConditions', {'Port1In','WaitForUS','Tup','Delay'},...
                'OutputActions', {'Serial1Code', Freq,'BNCState', 2});   % play tone
                %'OutputActions', {'BNCState', 2});   % play tone debugger
                
            sma = AddState(sma, 'Name','Delay', ... phase 2: if the animal responds in the delay time
                'Timer', S.Delay,...
                'StateChangeConditions', {'Port1In','WaitForUS','Tup',StateChangeArgument2},...
                'OutputActions', {'BNCState', 0}); %  no actual hit made, but reward is given 
            sma = AddState(sma, 'Name', 'WaitForUS', ...
                'Timer',S.RewardDelay,...
                'StateChangeConditions', {'Tup', StateChangeArgument1},...
                'OutputActions', {'BNCState', 0});   % reward/punishment is given after a random(Gauss) delay
            sma = AddState(sma,'Name', 'Reward', ...
                'Timer',S.RewardValveTime,...
                'StateChangeConditions', {'Tup', 'PostUS'},...
                'OutputActions', {'ValveState', S.RewardValveCode});   % deliver water
            sma = AddState(sma, 'Name', 'Punish', ...
                'Timer',S.PunishValveTime, ...
                'StateChangeConditions', {'Tup', 'PostUS'}, ...
                'OutputActions', {'ValveState', S.PunishValveCode}); %deiliver air puff
            sma = AddState(sma,'Name','PostUS',...
                'Timer',1,...
                'StateChangeConditions',{'Port1In','ResetDrinkingTimer','Tup','exit'},...
                'OutputActions',{});   % drinking
            sma = AddState(sma,'Name','ResetDrinkingTimer',...
                'Timer',0,...
                'StateChangeConditions',{'Tup','PostUS'},...
                'OutputActions',{});   % keep the animal in PostUS until licking stops for 1 s
            SendStateMatrix(sma);
            RawEvents = RunStateMatrix;
            if ~isempty(fieldnames(RawEvents)) % If trial data was returned
                BpodSystem.Data = AddTrialEvents(BpodSystem.Data,RawEvents); % Computes trial events from raw data
                BpodSystem.Data.TrialSettings(currentTrial) = S; % Adds the settings used for the current trial to the Data struct (to be saved after the trial ends)
                BpodSystem.Data.TrialTypes(currentTrial) = TrialTypes(currentTrial); % Adds the trial type of the current trial to data
                if TrialTypes(currentTrial)==3 
                BpodSystem.Data.TrialTypes(currentTrial) = TrialTypes(currentTrial)+r;
                end
                if length(S.SinWavekHz)==3
                BpodSystem.Data.thrdSound(currentTrial) = S.SinWavekHz(3);    
                end
                BpodSystem.Data.TotalReward=UpdateWaterReward(currentTrial,BpodSystem.Data,S.RewardAmount,S.GUI.FreeWater_sum);
                BpodSystem.Data.Hitnum=S.GUI.HitNum;
                BpodSystem.Data.TrialCode(currentTrial)=q;
                SaveBpodSessionData; % Saves the field BpodSystem.Data to the current data file
                if (S.GUI.Phase==2)
                   if isnan(BpodSystem.Data.RawEvents.Trial{currentTrial}.States.Delay(1))
                        S.GUI.HitNum=S.GUI.HitNum+1; % number of actual hits
                        GUIHandler('HitNum',S.GUI.HitNum)                       
                   end
                   BpodSystem.Data.Outcomes=UpdateOutcomePlot(1,TrialTypes, BpodSystem.Data, currentTrial);  
                elseif (S.GUI.Phase==3)
                   BpodSystem.Data.Outcomes=UpdateOutcomePlot(1,TrialTypes, BpodSystem.Data, currentTrial);
                                      if ~isnan(BpodSystem.Data.RawEvents.Trial{currentTrial}.States.Reward(1))
                                            S.GUI.HitNum=S.GUI.HitNum+1; % number of actual hits
                                            GUIHandler('HitNum',S.GUI.HitNum)                       
                                      end
                elseif (S.GUI.Phase==4 || S.GUI.Phase==5)
                BpodSystem.Data.Outcomes=UpdateOutcomePlot(S.TrialType,TrialTypes, BpodSystem.Data, currentTrial);
                end
                
            HandlePauseCondition; % Checks to see if the protocol is paused. If so, waits until user resumes.
            if BpodSystem.BeingUsed == 0
                return

            end      
    
                     if S.GUI.Phase==3 || S.GUI.Phase==4 || S.GUI.Phase==5 % phase 3: calculates hitratio
                        if TrialTypes(currentTrial)==1
                        if ~isnan(BpodSystem.Data.RawEvents.Trial{currentTrial}.States.Reward(1)) 
                        temp1(mod(c,50)+1)=1;
                        else
                        temp1(mod(c,50)+1)=0;
                        end
                        hitratio1=sum(temp1);
                        c=c+1;
                        end
                        
                        if TrialTypes(currentTrial)==2
                        if isnan(BpodSystem.Data.RawEvents.Trial{currentTrial}.States.Punish(1)) 
                        temp2(mod(d,50)+1)=1;
                        else
                        temp2(mod(d,50)+1)=0;
                        end
                        hitratio2=sum(temp2);
                        d=d+1;
                        end
                        
                        if TrialTypes(currentTrial)==3
                        if S.TrialType(3)==2
                        if isnan(BpodSystem.Data.RawEvents.Trial{currentTrial}.States.Punish(1)) 
                        temp3(mod(e,50)+1)=1;
                        else
                        temp3(mod(e,50)+1)=0;
                        end
                        hitratio3=sum(temp3);
                        e=e+1;
                        end
                        if S.TrialType(3)==1
                        if ~isnan(BpodSystem.Data.RawEvents.Trial{currentTrial}.States.Reward(1)) 
                        temp3(mod(e,50)+1)=1;
                        else
                        temp3(mod(e,50)+1)=0;
                        end
                        hitratio3=sum(temp3);
                        e=e+1;
                        end
                        end
                        
%                     if hitratio1>=S.HitRatio_req % if the hitratio is bigger than the requested percentage, move on to phase 4 
%                         %S.GUI.Phase=4;
%                         
%                     end
                    GUIHandler('HitRatio1',hitratio1/length(temp1)) %updates GUI
                    GUIHandler('HitRatio2',hitratio2/length(temp2)) %updates GUI
                    GUIHandler('HitRatio3',hitratio3/length(temp3))
                    GUIHandler('HitRatio_norm1',length(temp1))     %updates GUI
                    GUIHandler('HitRatio_norm2',length(temp2))
                    GUIHandler('HitRatio_norm3',length(temp3))
                    end
            S = BpodParameterGUI('sync', S);                   
            currentTrial=currentTrial+1;
    end

    end

function [Outcomes]=UpdateOutcomePlot(STrialType,TrialTypes, Data, currentTrial)
global BpodSystem
Outcomes=BpodSystem.Data.Outcomes;
x=Data.nTrials;
    if STrialType(TrialTypes(x))==1
    if ~isnan(Data.RawEvents.Trial{x}.States.Reward(1))
        if~isnan(BpodSystem.Data.RawEvents.Trial{x}.States.Delay(1))
            Outcomes(x) = 3; %blue circle
        else
            Outcomes(x) = 1; %full green circle
        end
    else
    Outcomes(x)=2;
    end
    end
    if STrialType(TrialTypes(x))==2
    if isnan(Data.RawEvents.Trial{x}.States.Punish(1))
        Outcomes(x)= -1;  % red circle
    else
        Outcomes(x)= 0;  % red full circle
    end
    end
TrialTypeOutcomePlot(BpodSystem.GUIHandles.OutcomePlot,'update',Data.nTrials+1,TrialTypes,Outcomes);

function [CalAmpl] = AmplAdjst(SPL,Tg,Ampl) % Calculate the new proper sinewave amplitude
    y = SPL - Tg;
    b =  20 * log10(Ampl) - y;
    c = b / 20;
    CalAmpl = 10 .^ c;
    
function ITI=expdelay(max)
        ITI = 5;
        while ITI > max   % ITI dustribution: 0.3 + exponential, truncated at 4 s
            ITI = exprnd(1)+0.3;
        end

function Delay=gaussdelay(mu,sigma,min,max)
        Delay=0;
        while (Delay<min | Delay>max)
        Delay=normrnd(mu,sigma);
        end
        
        
function soundinitializer (db,Fr,TeensyCalData,SoundDuration)
        n=find(TeensyCalData.Frequency==Fr);
        SPL = TeensyCalData.SPL(n); % Recalls calibrated dB for the frequency of tone 1
        Ampl = TeensyCalData.Amplitude(n); % Recalls calibrated amplitude for the tone 1 frequency
        NewAmpl  = AmplAdjst(SPL,db,Ampl); % Calculates new amplitude for tone 1
        sinewave  = NewAmpl.*sin(2*pi*Fr*1000/44100.*(0:44100*SoundDuration)); % Creates the sinewave of tone 1
        TeensySoundServer ('loadwaveform', Fr, sinewave); % Uploads the sinewave for tone 1
    
function [TotalReward]=UpdateWaterReward(currentTrial,Data,RewardAmount,FreeWater) % Sums up the water obtained
global BpodSystem
Outcomes = zeros(1,Data.nTrials);
for x = 1:Data.nTrials
    if ~isnan(BpodSystem.Data.RawEvents.Trial{x}.States.Reward(1))
        Outcomes(x) = RewardAmount;
    else
        Outcomes(x) = 0;    
    end
end
TotalReward = sum(Outcomes)+FreeWater*RewardAmount;
GUIHandler('Drinked_ul',TotalReward);


function GUIHandler(name,value)
global BpodSystem
    NumParam = BpodSystem.GUIData.ParameterGUI.nParams;
    for  n = 1:NumParam;
    if strcmp(BpodSystem.GUIData.ParameterGUI.ParamNames(n), name) == 1
    b = n;
    end
    end
    tmp = BpodSystem.GUIHandles.ParameterGUI.Params(b);
    set(tmp, 'String', num2str(value));       